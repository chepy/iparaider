#coding=utf-8
"""
	@name: IPA Raider 
	@author: Mrkelly
"""
import web
import os

from kk_lib import ipaWalker,ipaExtracter

urls = (
	'/', 'home',
	'/extract_all', 'extract_all',  # 解包所有在网站目录的ipa文件
)

render = web.template.render('kk_templates/', base='base' )

web_app = web.application( urls, globals() )

# 设置ipa目录
IPA_FILES_DIR = '%s/%s' % ( os.path.dirname(__file__), 'static/ipa_files/')
IPA_RESOURCES_DIR = '%s/%s' % ( os.path.dirname(__file__), 'static/ipa_resources/')

class home:
	def GET(self):
		return render.home()


class extract_all:
	"""
	将ipa_files中的所有ipa文件解压到ipa_resources
	"""
	def GET(self):
		# 获取所有/static/ipa_files/中所有的IPA文件
		ipa_files = os.listdir( IPA_FILES_DIR )
		
		# 开始解包
		for f in ipa_files:
			# 首先判断是不是ipa文件
			f_shortname, f_ext = os.path.splitext( f )
			if f_ext in ['.ipa']:
				ipa_extracter = ipaExtracter.IPAExtracter( '%s%s' %( IPA_FILES_DIR, f ) )  # 获取文件～
				ipa_extracter.extract_img( IPA_RESOURCES_DIR ) # 开始输出ipa中的东西
			
		return 'extract all!'

if __name__ == '__main__':
	"""
	主执行程序入口
	"""
	web_app.run()